package controllers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.bethecoder.ascii_table.ASCIITable;
import com.bethecoder.ascii_table.impl.CollectionASCIITableAware;
import com.bethecoder.ascii_table.spec.IASCIITableAware;
import com.google.common.base.Optional;

import asg.cliche.Command;
import asg.cliche.Shell;
import asg.cliche.ShellFactory;
import models.Activity;
import models.User;
import utils.BinarySerializer;
import utils.JSONSerializer;
import utils.Serializer;
import utils.XMLSerializer;
import asg.cliche.Param;

public class Main
{
  public PacemakerAPI paceApi;
  
  public Main() throws Exception
  {
   // XML Serializer
   File  datastore = new File("datastore.xml");
   Serializer serializer = new XMLSerializer(datastore);
    
    //JSON Serializer
    //File  datastore = new File("datastore.json");
    //Serializer serializer = new JSONSerializer(datastore);
    
    //Binary Serializer
    //File  datastore = new File("datastore.txt");
    //Serializer serializer = new BinarySerializer(datastore);

    paceApi = new PacemakerAPI(serializer);
    
    if (datastore.isFile())
    {
      paceApi.load();
    }
  }

  @Command(description="Create a new User")
  public void createUser (@Param(name="first name") String firstName, @Param(name="last name") String lastName, 
                          @Param(name="email")      String email,     @Param(name="password")  String password)
  {
    paceApi.createUser(firstName, lastName, email, password);
  }

  @Command(description="Get a Users details")
  public void getUser (@Param(name="email") String email)
  {
      User user = paceApi.getUserByEmail(email);
      System.out.println(user);
  }

  @Command(description="Get all users details")
  public void getUsers ()
  {
   // Collection<User> users = paceApi.getUsers();
   // System.out.println(users);
    // generate a List of Users from the Collection of users returned by our api. ascii-table expects a list
    List<User> userList = new ArrayList<User> (paceApi.getUsers());

    // Pass the list + the names of the field we wish to display. Note: there must be an accessor method in each 
    //  User object matching the names precisely
    IASCIITableAware asciiTableAware = new CollectionASCIITableAware<User>(userList, "firstname", "lastname", "email"); 

    // print the table
    ASCIITable.getInstance().printTable(asciiTableAware);
  }

  @Command(description="Delete a User")
  public void deleteUser (@Param(name="email") String email)
  {
    Optional<User> user = Optional.fromNullable(paceApi.getUserByEmail(email));
    if (user.isPresent())
    {
      paceApi.deleteUser(user.get().userId);
    }
  }
  
  @Command(description="Add an Activity")
  public void addActivity (@Param(name="userId")   String userId, @Param(name="type") String type, 
                           @Param(name="location") String location,     @Param(name="distance")  double distance)
  {  
    Optional<User> user = Optional.fromNullable(paceApi.getUser(userId));
    if (user.isPresent())
    {
      paceApi.createActivity(userId, type, location, distance);
      System.out.println("activity added");
    }
  }
  
  @Command(description="Add Location to an activity")
  public void addLocation (@Param(name="activity-id")  String  activityId,   
                           @Param(name="latitude")     float latitude, @Param(name="longitude") float longitude)
  {
    Optional<Activity> activity = Optional.fromNullable(paceApi.getActivity(activityId));
    if (activity.isPresent())
    {
      paceApi.addLocation(activity.get().activityId, latitude, longitude);
    }
  }
  
  public static void main(String[] args) throws Exception
  {
    Main main = new Main();
    Shell shell = ShellFactory.createConsoleShell("pm", "Welcome to pacemaker-console - ?help for instructions", main);
    shell.commandLoop();
    
    main.paceApi.store();
  }
}