package controllers;

import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.google.common.base.Optional;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import asg.cliche.Param;
import models.Activity;
import models.Location;
import models.User;
import utils.Serializer;

public class PacemakerAPI
{
  private Map<String, User> userIndex = new HashMap<>();
  private Map<String, User> emailIndex = new HashMap<>();
  private Map<String, Activity>     activitiesIndex       = new HashMap<>();
  private Serializer serializer;

  public PacemakerAPI(Serializer serializer)
  {
    this.serializer = serializer;
  }

  @SuppressWarnings("unchecked")
  public void load() throws Exception
  {
    serializer.read();
    activitiesIndex = (Map<String, Activity>) serializer.pop();
    emailIndex      = (Map<String, User>)   serializer.pop();
    userIndex       = (Map<String, User>)     serializer.pop();
  }

  public void store() throws Exception
  {
    serializer.push(userIndex);
    serializer.push(emailIndex);
    serializer.push(activitiesIndex);
    serializer.write(); 
  }
  
  public Collection<User> getUsers()
  {
    return userIndex.values();
  }

  public void deleteUsers()
  {
    userIndex.clear();
    emailIndex.clear();
  }

  public User createUser(String firstName, String lastName, String email, String password)
  {
    User user = new User(firstName, lastName, email, password);
    userIndex.put(user.userId, user);
    emailIndex.put(user.email, user);
    return user;
  }

  public User getUserByEmail(String email)
  {
    return emailIndex.get(email);
  }

  public User getUser(String userId)
  {
    return userIndex.get(userId);
  }

  public void deleteUser(String userId)
  {
    User user = userIndex.remove(userId);
    emailIndex.remove(user.email);
  }

  public Activity createActivity(String userId, String type, String location, double distance)
  {
    Activity activity = null;
    Optional<User> user = Optional.fromNullable(userIndex.get(userId));
    if (user.isPresent())
    {
      activity = new Activity (type, location, distance);
      user.get().activities.put(activity.activityId, activity);
      activitiesIndex.put(activity.activityId, activity);
    }
    return activity;
  }
  
  public Activity getActivity (String activityId)
  {
    return activitiesIndex.get(activityId);
  }
  
  public Location addLocation (String activityId, float latitude, float longitude)
  {
    Location location = null;
    Optional<Activity> activity = Optional.fromNullable(activitiesIndex.get(activityId));
    if (activity.isPresent())
    {
      location = new Location (latitude, longitude);
      activity.get().route.add(new Location( latitude, longitude));
    }
    return location;
  }
  
  @SuppressWarnings("unchecked")
  public void load(File file) throws Exception
  {
    ObjectInputStream is = null;
    try
    {
      XStream xstream = new XStream(new DomDriver());
      is = xstream.createObjectInputStream(new FileReader(file));
      userIndex       = (Map<String, User>)     is.readObject();
      emailIndex      = (Map<String, User>)   is.readObject();
      activitiesIndex = (Map<String, Activity>) is.readObject();
    }
    catch(EOFException e) 
    {
    }
    finally
    {
      if (is != null)
      {
        is.close();
      }
    }
  }

  public void store(File file) throws Exception
  {
    XStream xstream = new XStream(new DomDriver());
    ObjectOutputStream out = xstream.createObjectOutputStream(new FileWriter(file));
    out.writeObject(userIndex);
    out.writeObject(emailIndex);
    out.writeObject(activitiesIndex);
    out.close(); 
  }
  
  
}
