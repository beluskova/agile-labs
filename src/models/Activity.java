package models;

import static com.google.common.base.MoreObjects.toStringHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.google.common.base.Objects;

@SuppressWarnings("serial")
public class Activity implements Serializable
{
  {
}
    static Long   counter = 0l;

  //  public Long   activityId;
    
    public String activityId;
    public String type;
    public String location;
    public double distance;
    public List<Location> route = new ArrayList<>();

    public Activity()
    {
    }

    public Activity(String type, String location, double distance)
    {
     // this.activityId  = counter++;
      this.activityId     = UUID.randomUUID().toString();
      this.type        = type;
      this.location    = location;
      this.distance    = distance;
    }
    
    @Override
    public boolean equals(final Object obj)
    {
      if (obj instanceof Activity)
      {
        final Activity other = (Activity) obj;
        return Objects.equal(type, other.type) 
            && Objects.equal(location,  other.location)
            && Objects.equal(distance,  other.distance)
            && Objects.equal(route,     other.route);    
      }
      else
      {
        return false;
      }
    }
    
    @Override  
    public int hashCode()  
    {  
       return Objects.hashCode(this.activityId, this.type, this.location, this.distance);  
    }
    
    @Override
    public String toString()
    {
      return toStringHelper(this).addValue(activityId)
                                 .addValue(type)
                                 .addValue(location)
                                 .addValue(distance)   
                                 .addValue(route)
                                 .toString();
    }

}
