package models;

import static com.google.common.base.MoreObjects.toStringHelper;

import java.io.Serializable;
import java.util.UUID;

import com.google.common.base.Objects;

@SuppressWarnings("serial")
public class Location implements Serializable
{
  static Long counter = 0l;

 // public Long locationId;
  public String locationId;
  public float latitude;
  public float longitude;

  public Location()
  {
  }

  public Location(float latitude, float longitude)
  {
   // this.locationId = counter++;
    this.locationId     = UUID.randomUUID().toString();
    this.latitude = latitude;
    this.longitude = longitude;
  }

  @Override
  public boolean equals(final Object obj)
  {
    if (obj instanceof Location)
    {
      final Location other = (Location) obj;
      return Objects.equal(latitude, other.latitude) 
          && Objects.equal(longitude, other.longitude);
    }
    else
    {
      return false;
    }
  }
  
  @Override
  public int hashCode()
  {
   return Objects.hashCode(this.locationId, this.latitude, this.longitude);
  }

  @Override
  public String toString()
  {
    return toStringHelper(this)
                               .addValue(locationId)
                               .addValue(latitude)
                               .addValue(longitude)
                               .toString();
  }
}
