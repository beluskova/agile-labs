package models;

import static com.google.common.base.MoreObjects.toStringHelper;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.google.common.base.Objects;

@SuppressWarnings("serial")
public class User implements Serializable
{ 
  static Long   counter = 0l;

 // public Long   userId;
  public String userId;
  public String firstName;
  public String lastName;
  public String email;
  public String password;
 
  public Map<String, Activity> activities = new HashMap<>();

  public User()
  {
  }

  public User(String firstName, String lastName, String email, String password)
  {
    //this.userId     = counter++;
    this.userId     = UUID.randomUUID().toString();
    this.firstName  = firstName;
    this.lastName   = lastName;
    this.email      = email;
    this.password   = password;
  }
  
  @Override
  public boolean equals(final Object obj)
  {
    if (obj instanceof User)
    {
      final User other = (User) obj;
      return Objects.equal(firstName,   other.firstName) 
          &&  Objects.equal(lastName,    other.lastName)
          &&  Objects.equal(email,       other.email)
          &&  Objects.equal(password,    other.password)
          &&  Objects.equal(activities,  other.activities);      
    }
    else
    {
      return false;
    }
  }
  
  @Override  
  public int hashCode()  
  {  
     return Objects.hashCode(this.userId, this.lastName, this.firstName, this.email, this.password);  
  }
  
  @Override
  public String toString()
  {
    return toStringHelper(this).addValue(userId)
                               .addValue(firstName)
                               .addValue(lastName)
                               .addValue(password)
                               .addValue(email)  
                               .addValue(activities)
                               .toString();
  } 
  
  public String getFirstname() 
  {
    return firstName;
  }

  public String getLastname() 
  {
    return lastName;
  }

  public String getEmail() 
  {
    return email;
  }
}